<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <!-- responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- master stylesheet -->
    <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
    <!-- Responsive stylesheet -->
    <link href="{{ asset('front/plugins/revolution/css/settings.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/plugins/revolution/css/layers.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('front/plugins/revolution/css/navigation.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('front/css/responsive.css') }}">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('front/images/favicon/apple-touch-icon.html') }}">
    <link rel="icon" type="image/png" href="{{ asset('front/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('front/images/favicon/favicon-16x16.png') }}" sizes="16x16">

    <!-- Fixing Internet Explorer-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="{{ asset('front/js/html5shiv.js') }}"></script>
    <![endif]-->

</head>
<body>
<div class="boxed_wrapper">


    <!--Start Preloader -->
    <div class="preloader"></div>
    <!--End Preloader -->

    <!--Start Top bar area -->
    <section class="top-bar-area">
        @include('front.includes.top-bar')
    </section>
    <!--End Top bar area -->

    <!--Start header area-->
    <header class="header-area">
       @include('front.includes.header')
    </header>
    <!--End header area-->


    <!--Start mainmenu area-->
    <section class="mainmenu-area stricky">
        @include('front.includes.main-menu')
    </section>
    <!--End mainmenu area-->

    @yield('content')

    <!--Start footer area-->
    <footer class="footer-area">
        @include('front.includes.footer')
    </footer>
    <!--End footer area-->


    <!--Start footer bottom area-->
    <section class="footer-bottom-area">
        @include('front.includes.copyright')
    </section>
    <!--End footer bottom area-->


    <!--Scroll to top-->
    <div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>

    <!-- main jQuery -->
    <script src="{{ asset('front/js/jquery-1.11.1.min.js') }}"></script>
    <!-- bootstrap -->
    <script src="{{ asset('front/js/bootstrap.min.js') }}"></script>
    <!-- count to -->
    <script src="{{ asset('front/js/jquery.countTo.js') }}"></script>
    <!-- owl carousel -->
    <script src="{{ asset('front/js/owl.carousel.min.js') }}"></script>
    <!-- validate -->
    <script src="{{ asset('front/js/validation.js') }}"></script>
    <!-- mixit up -->
    <script src="{{ asset('front/js/jquery.mixitup.min.js') }}"></script>
    <!-- fancy box -->
    <script src="{{ asset('front/js/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('front/js/jquery.appear.js') }}"></script>
    <!-- isotope script-->
    <script src="{{ asset('front/js/isotope.js') }}"></script>
    <script src="{{ asset('front/js/lightbox.js') }}"></script>
    <script src="{{ asset('front/js/jquery.magnific-popup.min.js') }}"></script>
    <!-- Bootstrap select picker js -->
    <script src="{{ asset('front/assets/bootstrap-sl-1.12.1/bootstrap-select.js') }}"></script>
    <!-- Bootstrap bootstrap touchspin js -->
    <!-- jQuery ui js -->
    <script src="{{ asset('front/assets/jquery-ui-1.11.4/jquery-ui.js') }}"></script>

    <!--Revolution Slider-->
    <script src="{{ asset('front/plugins/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('front/plugins/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="{{ asset('front/js/main-slider-script.js') }}"></script>



    <!-- thm custom script -->
    <script src="{{ asset('front/js/custom.js') }}"></script>

</div>
</body>
</html>