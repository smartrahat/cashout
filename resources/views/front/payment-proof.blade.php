@extends('layouts.front')

@section('title','Payment Proof')

@section('content')
    <!--Page Title-->
    <section class="page-title" style="background: url({{ asset('front/images/resources/banner-bg.jpg') }});">
        <div class="container text-center">
            <h2>Payment Proof</h2>
            <ul class="title-manu">
                <li><a href="{{ action('FrontController@index') }}">home</a></li>
                <li>//</li>
                <li>Payment Proof</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="title">Buy Proof</h1>
                <table class="table table-bordered table-hover text-white">
                    <thead>
                    <tr class="title-head">
                        <th>Date</th>
                        <th>Name</th>
                        <th>Method</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$50</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$10</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$40</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$50</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$10</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$50</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$5</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$22</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$18</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$15</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$150</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$100</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$120</td>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$80</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$55</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$60</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$25</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$45</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$22</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$96</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$50</td>
                    </tr>
                    </tbody>
                </table>



            </div>
            <div class="col-md-6">
                <h1 class="title">Sell Proof</h1>
                <table class="table table-bordered table-hover text-white">
                    <thead>
                    <tr class="title-head">
                        <th>Date</th>
                        <th>Name</th>
                        <th>Method</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$50</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$10</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$40</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$50</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$10</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$50</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$5</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$22</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$18</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$15</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$150</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$100</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$120</td>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$80</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$55</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$60</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$25</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$45</td>
                    </tr>
                    <tr class="bg-success">
                        <th scope="row">2018-10-24</th>
                        <td>Mark</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$22</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$96</td>
                    </tr>
                    <tr class="bg-info">
                        <th scope="row">2018-10-24</th>
                        <td>Jacob</td>
                        <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt=""></td>
                        <td>$50</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
@stop