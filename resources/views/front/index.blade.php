@extends('layouts.front')

@section('title','Dollar Buy Sell Exchange')

@section('content')

    <!--Main Slider-->
    <section class="main-slider">
        @include('front.includes.slider')
    </section>
    <!--End Main Slider-->

    <!--Our Latest transaction Section Start -->
    <section class="transaction-section">
        @include('front.includes.latest-transection')
    </section>
    <!--Our Latest transaction Section End -->

    <!--Start Service Section-->
    <section class="service-section">
        @include('front.includes.services')
    </section>
    <!--End Service Section-->

    <!--Start contact area-->
    <section class="contact-area">
        @include('front.includes.contact-ribbon')
    </section>
    <!--End contact area-->

    <!--Start service style 2-->
    <section  id="service-section" class="service-section style2">
        @include('front.includes.service-2')
    </section>
    <!--End service style 2-->

    <section class="accept-section">
        @include('front.includes.payment-methods')
    </section>

@stop