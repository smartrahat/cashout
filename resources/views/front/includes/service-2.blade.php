<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="sec-title text-center">
                <h2>our awsome services</h2>
                <span class="border"></span>
                <p>Lorem ipsum dolor  amet mi ultricies interdum pede eu vestibulum vulputate maurimtum <br>commod rhoncus consectetuer reduce producet</p>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="service-block text-left">
                <div class="icon-area">
                    <div class="icon-box">
                        <figure><img src="{{ asset('front/images/services/services-1.jpg') }}" alt="services-1"></figure>
                        <i class="flaticon-car-directions"></i>
                    </div>
                    <div class="service-det">
                        <a href="{{ asset('front/services-details.html') }}"><h5>Dolla Buy, Sell, Exchange</h5></a>
                        <p>Adipiscing laoreet dui sed sceleri enas pellentesque ac justo. Lectu feltrumn Placerat arcu dolor laoreet</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="service-block text-left">
                <div class="icon-area">
                    <div class="icon-box">
                        <figure><img src="{{ asset('front/images/services/services-2.jpg') }}" alt="services-1"></figure>
                        <i class="flaticon-crawling-baby-silhouette"></i>
                    </div>
                    <div class="service-det">
                        <a href="{{ asset('front/services-details.html') }}"><h5>Instant Payment</h5></a>
                        <p>Adipiscing laoreet dui sed sceleri enas pellentesque ac justo. Lectu feltrumn Placerat arcu dolor laoreet</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="service-block text-left">
                <div class="icon-area">
                    <div class="icon-box">
                        <figure><img src="{{ asset('front/images/services/services-3.jpg') }}" alt="services-1"></figure>
                        <i class="flaticon-money"></i>
                    </div>
                    <div class="service-det">
                        <a href="{{ asset('front/services-details.html') }}"><h5>Quick Reply</h5></a>
                        <p>Adipiscing laoreet dui sed sceleri enas pellentesque ac justo. Lectu feltrumn Placerat arcu dolor laoreet</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="service-block text-left">
                <div class="icon-area">
                    <div class="icon-box">
                        <figure><img src="{{ asset('front/images/services/services-4.jpg') }}" alt="services-1"></figure>
                        <i class="flaticon-seo-performance-marketing-graphic"></i>
                    </div>
                    <div class="service-det">
                        <a href="services-details.html"><h5>1% Referral Commission System</h5></a>
                        <p>Adipiscing laoreet dui sed sceleri enas pellentesque ac justo. Lectu feltrumn Placerat arcu dolor laoreet</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="service-block text-left">
                <div class="icon-area">
                    <div class="icon-box">
                        <figure><img src="{{ asset('front/images/services/services-5.jpg') }}" alt="services-1"></figure>
                        <i class="flaticon-gmo"></i>
                    </div>
                    <div class="service-det">
                        <a href="services-details.html"><h5>Direct Referral Income</h5></a>
                        <p>Adipiscing laoreet dui sed sceleri enas pellentesque ac justo. Lectu feltrumn Placerat arcu dolor laoreet</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="service-block text-left">
                <div class="icon-area">
                    <div class="icon-box">
                        <figure><img src="{{ asset('front/images/services/services-6.jpg') }}" alt="services-1"></figure>
                        <i class="flaticon-support"></i>
                    </div>
                    <div class="service-det">
                        <a href="services-details.html"><h5>24 Hours Reply</h5></a>
                        <p>Adipiscing laoreet dui sed sceleri enas pellentesque ac justo. Lectu feltrumn Placerat arcu dolor laoreet</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>