<div class="container">
    <div class="mainmenu-bg">
        <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <!--Start mainmenu-->
                <nav class="main-menu">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li class="home current"><a href="{{ action('FrontController@index') }}">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#service-section">Services</a></li>
                            <li><a href="{{ action('FrontController@faqs') }}">FAQs</a></li>
                            <li><a href="{{ action('FrontController@paymentProof') }}">Payment Proof</a></li>
                            <li><a href="{{ action('FrontController@contact') }}">Contact</a></li>
                        </ul>
                    </div>
                </nav>
                <!--End mainmenu-->

            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="free-consulation-button pull-right">
                    <a class="thm-btn" href="#">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
</div>