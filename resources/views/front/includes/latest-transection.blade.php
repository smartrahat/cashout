<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="sec-title text-center">
                <h2> LATEST TRANSACTION </h2>
                <span class="border"></span>
            </div>
            <table class="table table-bordered table-hover text-white">
                <thead>
                <tr class="title-head">
                    <th>SL</th>
                    <th>Send</th>
                    <th>Receive</th>
                    <th>Amount</th>
                    <th>Date</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <tr class="bg-info">
                    <th scope="row">1</th>
                    <td> <img class="payment-proof-img" src="{{ asset('front/images/slides/2.jpg') }}" alt="">
                        Coinbase
                    </td>
                    <td> <img class="payment-proof-img" src="{{ asset('front/images/payment proof/bKash.jpg') }}" alt="">
                        BKash</td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">2</th>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/perfect_money.jpg') }}" alt="">
                        Perfect Money
                    </td>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/neteller.jpg') }}" alt="">
                        Neteller
                    </td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">3</th>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/bitcoin.jpg') }}" alt="">
                        Bitcoin
                    </td>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt="">
                        Coinbase
                    </td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">4</th>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/neteller.jpg') }}" alt="">
                        Neteller
                    </td>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/perfect_money.jpg') }}" alt="">
                        Perfect Money
                    </td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">5</th>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/skrill.jpg') }}" alt="">
                        Skrill
                    </td>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/neteller.jpg') }}" alt="">
                        Neteller
                    </td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">6</th>
                    <td> <img class="payment-proof-img" src="{{ asset('front/images/payment proof/rocket.jpg') }}" alt="">
                        Rocket
                    </td>
                    <td> <img class="payment-proof-img" src="{{ asset('front/images/payment proof/bKash.jpg') }}" alt="">
                        BKash</td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">7</th>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/neteller.jpg') }}" alt="">
                        Neteller
                    </td>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/perfect_money.jpg') }}" alt="">
                        Perfect Money
                    </td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">8</th>
                    <td> <img class="payment-proof-img" src="{{ asset('front/images/payment proof/rocket.jpg') }}" alt="">
                        Rocket
                    </td>
                    <td> <img class="payment-proof-img" src="{{ asset('front/images/payment proof/bKash.jpg') }}" alt="">
                        BKash</td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">9</th>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/bitcoin.jpg') }}" alt="">
                        Bitcoin
                    </td>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/neteller.jpg') }}" alt="">
                        Neteller
                    </td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>
                <tr class="bg-info">
                    <th scope="row">10</th>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/bKash.jpg') }}" alt="">
                        Bkash
                    </td>
                    <td><img class="payment-proof-img" src="{{ asset('front/images/payment proof/coinbase.jpg') }}" alt="">
                        Coinbase
                    </td>
                    <td>$100</td>
                    <td>2018-10-24</td>
                    <td>Processed</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>