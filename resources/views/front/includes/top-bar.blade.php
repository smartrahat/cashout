<div class="container clearfix">
    <div class="left-side">
        <div class="top-info">
            <li class="disabled block d-none d-md-block"><a href="#"><i class="fa fa-clock-o"></i> Wednesday, September 19, 2018 </a></li>
        </div>
    </div>
    <div class="right-side clearfix">
        <div class="top-right">
            <h6>Follow Us</h6>
        </div>
        <ul class="social-links">
            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Log In</a></li>
        </ul>
    </div>
</div>