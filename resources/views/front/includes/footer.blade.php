<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="footer-social-block">
                <h3 style="padding-bottom: 20px;">find us via social media </h3>
                <ul class="footersocial">
                    <li><a href="https://www.facebook.com/"><i class="fa fa-facebook-f"></i></a></li>
                    <li><a href="https://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.skype.com"><i class="fa fa-skype"></i></a></li>
                    <li><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p"></i></a></li>
                    <li><a href="https://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.youtube.com"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>