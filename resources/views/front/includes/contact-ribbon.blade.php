<div class="container">
    <div class="row">
        <!--Start heading item-->
        <div class="col-md-10 col-sm-8 col-xs-12">
            <div class="heading-text text-left">
                <h1> Contact with us for Dollar Buy, Sell & Exchange Solution </h1>
            </div>
        </div>
        <!--End single item-->

        <!--Start btn item-->
        <div class="col-md-2 col-sm-4 col-xs-12">
            <div class="btn-cont text-right">
                <a href="contact.html" class="thm-btn bg-clr2">Contact Us</a>
            </div>
        </div>
    </div>
</div>
