<div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="logo">
                <a href="{{ action('FrontController@index') }}">
                    <img src="{{ asset('front/images/resources/logo.png') }}" alt="Awesome Logo">
                </a>
            </div>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12">
            <div class="header-contact-info">
                <ul>
                    <li>
                        <div class="iocn-holder">
                            <span class="flaticon-pin"></span>
                        </div>
                        <div class="text-holder">
                            <h6>Address</h6>
                            <p>Address</p>
                        </div>
                    </li>
                    <li>
                        <div class="iocn-holder">
                            <span class="flaticon-call"></span>
                        </div>
                        <div class="text-holder">
                            <h6>Call Free</h6>
                            <p>+880 1710 000 000</p>
                        </div>
                    </li>
                    <li>
                        <div class="iocn-holder">
                            <span class="flaticon-clock"></span>
                        </div>
                        <div class="text-holder">
                            <h6>Mon - Sun</h6>
                            <p>Friday Closed</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>