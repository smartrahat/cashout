<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="sec-title text-center">
                <h2>why choose us</h2>
                <span class="border"></span>
                <p>Lorem ipsum dolor  amet mi ultricies interdum pede eu vestibulum vulputate maurimtum <br>commod rhoncus consectetuer reduce producet</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="service-block text-center">
                <div class="icon-area">
                    <div class="icon-box">
                        <i class="flaticon-cute-rocket-launching"></i>
                    </div>
                    <a href="#"><h5>BUY Dollar</h5></a>
                    <p>You can Buy maximum of $ 100 USD.You can Buy minimum of $ 5 USD</p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="service-block text-center">
                <div class="icon-area">
                    <div class="icon-box">
                        <i class="flaticon-support"></i>
                    </div>
                    <a href="#"><h5>SELL Dollar</h5></a>
                    <p>You can Sell maximum of $ 100 USD. You can Sell minimum of $ 5 USD </p>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="service-block text-center">
                <div class="icon-area">
                    <div class="icon-box">
                        <i class="flaticon-seo-performance-marketing-graphic"></i>
                    </div>
                    <a href="#"><h5>EXCHANGE Dollar</h5></a>
                    <p>You can Exchange max of $0 USD.You can Exchange min of $0 USD</p>
                </div>
            </div>
        </div>
    </div>
</div>