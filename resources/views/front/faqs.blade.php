@extends('layouts.front')

@section('title','FAQs')

@section('content')

    <!--Page Title-->
    <section class="page-title" style="background: url({{ asset('front/images/resources/banner-bg.jpg') }});">
        <div class="container text-center">
            <h2>Faq's</h2>
            <ul class="title-manu">
                <li><a href="{{ action('FrontController@index') }}">home</a></li>
                <li>//</li>
                <li>FAQs</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!--Start service style 2-->
    <section class="faq">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <!--Start single accordion box-->
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num">01</i>
                            <h4>Which do you like better, curtains or blinds?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>

                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num">02</i>
                            <h4>If you didn't need sleep, what would you do?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num">03</i>
                            <h4>If you didn't need sleep, what would you do?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num">04</i>
                            <h4>If you didn't need sleep, what would you do?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num border">05</i>
                            <h4>Have you ever been in a tree house?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <!--Start single accordion box-->
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num">06</i>
                            <h4>Have you ever cheated on a test?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>

                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num">07</i>
                            <h4>Which do you like better, pancakes or waffles?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num">08</i>
                            <h4>Which do you like better, pancakes or waffles?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num">09</i>
                            <h4>Which do you like better, pancakes or waffles?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>
                    <div class="accordion accordion-block">
                        <div class="accord-btn">
                            <i class="left-num border">10</i>
                            <h4>What is the strangest thing you've eaten?</h4>
                        </div>
                        <div class="accord-content">
                            <p>Lorem ipsum dolor sit amet, laoreet mattis, vivamus morbi eleifend nec montes mollis, massa lacinia nullam.Sed ut justo sodalursus neque nascetmagna urna eget. Ante velit sed nullam.</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--End service style 2-->

@stop