<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        return view('front.index');
    }

    public function faqs()
    {
        return view('front.faqs');
    }

    public function paymentProof()
    {
        return view('front.payment-proof');
    }

    public function contact()
    {
        return view('front.contact');
    }
}
